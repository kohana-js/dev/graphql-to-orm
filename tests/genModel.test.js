const { buildSchema } = require('graphql');
const {schemaHeader} = require('graphql-to-sqlite-ddl');
const {codeGen} = require('../index');

describe('test schema to model', () => {
  test('simple schema', () => {
    const schema = buildSchema(`
type Persons {
    first_name: String!
    last_name: String!
    phone: String
    email: String
}`);

    const model = codeGen(schema);
    const target =
`const {ORM} = require('kohanajs');

class Person extends ORM{
  first_name = null;
  last_name = null;
  phone = null;
  email = null;

  static joinTablePrefix = 'person';
  static tableName = 'persons';

  static fields = new Map([
    ["first_name", "String!"],
    ["last_name", "String!"],
    ["phone", "String"],
    ["email", "String"]
  ]);
}

module.exports = Person;
`

    expect([...model.values()].join("")).toBe(target);

  })

  test('default value', () => {
    const schema = buildSchema(schemaHeader + `
type Persons {
    name: String!
    foo: Boolean! @default(value: true)
    koo: Boolean! @default(value: false)
    bar: String! @default(value: "yoo")
    tar: Float! @default(value: 0.5)
    haa: Int! @default(value: 100)
}`);

    const model = codeGen(schema);
    const target =
      `const {ORM} = require('kohanajs');

class Person extends ORM{
  name = null;
  foo = true;
  koo = false;
  bar = "yoo";
  tar = 0.5;
  haa = 100;

  static joinTablePrefix = 'person';
  static tableName = 'persons';

  static fields = new Map([
    ["name", "String!"],
    ["foo", "Boolean!"],
    ["koo", "Boolean!"],
    ["bar", "String!"],
    ["tar", "Float!"],
    ["haa", "Int!"]
  ]);
}

module.exports = Person;
`

    expect([...model.values()].join("")).toBe(target);

  })

  test('foreign Keys', ()=>{
    const schema = buildSchema(schemaHeader + `
type Users{
    name: String
}

type Blogs{
    handle: String 
    belongsTo: Users @foreignKey(value: "owner_id")
    
}
 `);

    const model = codeGen(schema);
    const target =
`const {ORM} = require('kohanajs');

class User extends ORM{
  name = null;

  static joinTablePrefix = 'user';
  static tableName = 'users';

  static fields = new Map([
    ["name", "String"]
  ]);
  static hasMany = [
    ["owner_id", "Blog"]
  ];
}

module.exports = User;
const {ORM} = require('kohanajs');

class Blog extends ORM{
  owner_id = null;
  handle = null;

  static joinTablePrefix = 'blog';
  static tableName = 'blogs';

  static fields = new Map([
    ["handle", "String"]
  ]);
  static belongsTo = new Map([
    ["owner_id", "User"]
  ]);
}

module.exports = Blog;
`

    expect([...model.values()].join("")).toBe(target);
  })

  test('hasAndBelongsToMany', ()=>{
    const schema = buildSchema(schemaHeader + `
type Product{
    name: String
}

type Collections{
    name: String 
    hasAndBelongsToMany: Product
}
 `);

    const target =
`const {ORM} = require('kohanajs');

class Product extends ORM{
  name = null;

  static joinTablePrefix = 'product';
  static tableName = 'products';

  static fields = new Map([
    ["name", "String"]
  ]);
}

module.exports = Product;
const {ORM} = require('kohanajs');

class Collection extends ORM{
  name = null;

  static joinTablePrefix = 'collection';
  static tableName = 'collections';

  static fields = new Map([
    ["name", "String"]
  ]);
  static belongsToMany = new Set([
    "Product"
  ]);
}

module.exports = Collection;
`;

    const model = codeGen(schema);
    expect([...model.values()].join("")).toBe(target);
  })

  test('JSON type', ()=>{
      const schema = buildSchema(schemaHeader + `
type Person {
  name: String,
  meta: JSON
}`);

    const target =
`const {ORM} = require('kohanajs');

class Person extends ORM{
  name = null;
  meta = null;

  static joinTablePrefix = 'person';
  static tableName = 'persons';

  static fields = new Map([
    ["name", "String"],
    ["meta", "JSON"]
  ]);
}

module.exports = Person;
`

    const model = codeGen(schema);
    expect([...model.values()].join("")).toBe(target);
  })

  test('Customer, addresses and customer attribute', ()=>{
    const schema = buildSchema(schemaHeader + `
type Customers {
  username : String!
}

type Addresses {
  company : String
  belongsTo: Customers
}

type CustomerAttributes{
  value: String
  belongsTo : Customers
}
    `);

    const target =
`const {ORM} = require('kohanajs');

class Customer extends ORM{
  username = null;

  static joinTablePrefix = 'customer';
  static tableName = 'customers';

  static fields = new Map([
    ["username", "String!"]
  ]);
  static hasMany = [
    ["customer_id", "Address"],
    ["customer_id", "CustomerAttribute"]
  ];
}

module.exports = Customer;
const {ORM} = require('kohanajs');

class Address extends ORM{
  customer_id = null;
  company = null;

  static joinTablePrefix = 'address';
  static tableName = 'addresses';

  static fields = new Map([
    ["company", "String"]
  ]);
  static belongsTo = new Map([
    ["customer_id", "Customer"]
  ]);
}

module.exports = Address;
const {ORM} = require('kohanajs');

class CustomerAttribute extends ORM{
  customer_id = null;
  value = null;

  static joinTablePrefix = 'customer_attribute';
  static tableName = 'customer_attributes';

  static fields = new Map([
    ["value", "String"]
  ]);
  static belongsTo = new Map([
    ["customer_id", "Customer"]
  ]);
}

module.exports = CustomerAttribute;
`;

    const model = codeGen(schema);
    expect([...model.values()].join("")).toBe(target);
  })

  test('default belongsTo', ()=>{
    const schema = buildSchema(schemaHeader + `
type Users{
    name: String
}

type Blogs{
    handle: String 
    belongsTo: Users @default(value: 1)
}
 `);
    const model = codeGen(schema);
    const target =
      `const {ORM} = require('kohanajs');

class User extends ORM{
  name = null;

  static joinTablePrefix = 'user';
  static tableName = 'users';

  static fields = new Map([
    ["name", "String"]
  ]);
  static hasMany = [
    ["user_id", "Blog"]
  ];
}

module.exports = User;
const {ORM} = require('kohanajs');

class Blog extends ORM{
  user_id = 1;
  handle = null;

  static joinTablePrefix = 'blog';
  static tableName = 'blogs';

  static fields = new Map([
    ["handle", "String"]
  ]);
  static belongsTo = new Map([
    ["user_id", "User"]
  ]);
}

module.exports = Blog;
`

    expect([...model.values()].join("")).toBe(target);
  })

  test('default associateTo', ()=>{
    const schema = buildSchema(schemaHeader + `
type Users{
    name: String
}

type Blogs{
    handle: String 
    associateTo: Users @default(value: 1)
}
 `);
    const model = codeGen(schema);
    const target =
      `const {ORM} = require('kohanajs');

class User extends ORM{
  name = null;

  static joinTablePrefix = 'user';
  static tableName = 'users';

  static fields = new Map([
    ["name", "String"]
  ]);
  static hasMany = [
    ["user_id", "Blog"]
  ];
}

module.exports = User;
const {ORM} = require('kohanajs');

class Blog extends ORM{
  user_id = 1;
  handle = null;

  static joinTablePrefix = 'blog';
  static tableName = 'blogs';

  static fields = new Map([
    ["handle", "String"]
  ]);
  static belongsTo = new Map([
    ["user_id", "User"]
  ]);
}

module.exports = Blog;
`

    expect([...model.values()].join("")).toBe(target);
  })
});